# Event picking made easy

This is a scriptification of TWiki-Page instructions for a
fire-and-forget approach to event picking.

## Step 0: Define events

Define the events you want to pick by creating a file (e.g. `events.txt`) with one event per line, each line given as `runnumber eventnumber`.

## Step 1: Gather the data

Setup EventIndex, rucio and get a VOMS proxy

    setupATLAS
    lsetup rucio
    lsetup eiclient
    lsetup pyami
    voms-proxy-init -voms atlas
    
Find an RSE where you want to store the files, for example your LOCALGROUPDISK.

Also, find the derivation format you're using (e.g. DAOD_HIGG3D1). Passing this along will allow the script to find the correct AMI tag for you.

Now, call the script to collect the files:

    python gatherFiles.py --list events.txt --rse MYLOCALGROUPDISK --type DAOD_HIGG3D1 --write eventinfo.txt
    
This will run a while to locate the RAW files containing your events
and create rules to replicate them to your RSE.  You can follow the
process on this website: https://rucio-ui.cern.ch/r2d2

The command will print a `rucio download` command at the end. Make
sure to save that until your data is fully replicated. Once it is, you
can start with Step 2.

## Step 2: Pick & reconstruct the events

Download your data using the command you saved in the last step.

Then, setup a suitable release

    setupATLAS
    asetup Athena,21.0,latest

Once you have it, call the script to pick the events.

    bash pickEvents.sh eventinfo.txt /path/to/where/you/downloaded/the/data
    
This will create reco-scripts, called

    reco-RUNNUMBER-EVENTNUMBER.sh
    
These contain the `Reco_tf.py` call needed to reconstruct the events.
Create a new shell, so that each of the reco scripts can setup its own
environment correctly. Then, you can execute them like this:

    for file in $(ls reco_*.sh); do bash $file; done
    
This will produce the ESDs for you

    event-RUNNUMBER-EVENTNUMBER.pool.root

## Step 3: Create your event displays

Setup a suitable release, for example

    setupATLAS
    asetup Athena,22.0.0
    
Then, you have access to VP1

    vp1 event-RUNNUMBER-EVENTNUMBER.pool.root

If that doesn't work, it might help to use an ATLAS docker container:

   chmod 777 .
   docker run --privileged -i -t --env DISPLAY=unix$DISPLAY -v $HOME/.Xauthority:/root/.Xauthority -v /tmp/.X11-unix:/tmp/.X11-unix -v /cvmfs:/cvmfs -v $HOME:$HOME -v $PWD:/home/atlas/workdir atlas/atlas_external_cvmfs
   setupATLAS
   asetup Athena,22.0.0
   cd workdir


    


