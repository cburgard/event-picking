#!/bin/bash

if [ $# -ne 2 ]; then
   echo "usage: pickEvents events.txt /path/to/the/files"
   exit
fi

while read line; do
    elements=($line)
    fname=${elements[0]}-${elements[1]}
    if [ ! -f transform-$fname.log ]; then
        GetTfCommand.py --AMI ${elements[2]} > transform-$fname.log
    fi
    if [ ! -f reco-$fname.sh ]; then
	echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase" >  reco-$fname.sh
        echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh"  >> reco-$fname.sh
	grep asetup transform-$fname.log >> reco-$fname.sh
	echo "if [ ! -f event-$fname.RAW.data ]; then" >> reco-$fname.sh
	echo "echo ${elements[0]} ${elements[1]} > evt.txt" >> reco-$fname.sh
        echo "acmd.py filter-files -s evt.txt "$(find $2 -name ${elements[3]})" -o event-$fname.RAW.data" >>  reco-$fname.sh
	echo "fi" >> reco-$fname.sh	
	echo "if [ ! -f event-$fname.pool.root ]; then" >> reco-$fname.sh
	grep Reco_tf.py transform-$fname.log | sed -e "s/\\\'//g" | tr -d '\n' >> reco-$fname.sh
        echo " --inputBSFile event-$fname.RAW.data --outputESDFile event-$fname.pool.root" >> reco-$fname.sh
	echo "fi" >> reco-$fname.sh
    fi
done < $1
