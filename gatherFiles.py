#!/usr/bin/env python3

def sysexec(cmd):
    """shell-execute a command"""
    import subprocess
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    return process.communicate()

def get_rtag(runno,dstype):
    from pyAMI.client import Client
    ami = Client("atlas")
    from pyAMI.utils import smart_execute
    datasets = smart_execute(ami, "datasets", patterns = None, fields = "ami_tags", order = None, limit = 1, show_archived = False,
                             run_number=str(runno),type=dstype,stream="physics_Main")
    tagstring = datasets.get_rows()[0]["ami_tags"]
    tags = tagstring.split("_")
    rtags = [ t for t in tags if t[0] == 'r' ]
    ftags = [ t for t in tags if t[0] == 'f' ]
    if len(rtags) > 0:
        return rtags[0]
    if len(ftags) > 0:
        return ftags[0]
    return tagstring

def get_guid(runno,evtno,regex,remote=False):
    """get the GUID based on runnumber and eventnumber. selects the container matching the regex."""

    if remote:
        url = "http://aiatlas073.cern.ch/EIHadoop/EI.jsp"
        import json
        import requests
        data = {
            "e":'{:d}+{:d}'.format(runno,evtno),
            "s":"physics_Main",
            "d":"AOD",
            "details":"type+id",
            "t":"RAW",
            "p":"all"
        }
                
        r = requests.post(url, data = json.dumps(data), timeout = 10, verify=False)
        if r.status_code == 200:
            try:
                start = r.text.index("###START###")
                stop  = r.text.index("###STOP###")
                output = r.text[start + 11 : stop]
            except ValueError:
                raise RuntimeError("unable to parse "+r.text)
        else:
            raise RuntimeError("HTTP Error {:d}: {:s}".format(r.status_code, r.reason))
    else:
        import os
        if not "EIDIR" in os.environ.keys():
            raise RuntimeError("you have to setup EventIndex before you can use this script!")
        cmd = ['java','-jar',os.path.join(os.environ['EIDIR'],'lib','EIHadoopEL.exe.jar'),'-e','{:d} {:d}'.format(runno,evtno),'-s','physics_Main','-details','type id','-t','RAW','-p','all']
        cmd += ['-d','AOD']
        output,error = sysexec(cmd)

    for line in output.split("\n"):
        if not line: continue
        try:
            guid,stream,container = line.split()
        except ValueError:
            raise RuntimeError("malformed output: "+output)
        if regex.match(container):
            return guid

def add_rule(elements,rse,comment):
    """add a rucio rule"""
    import rucio.client
    import rucio.common.exception
    try:
        client=rucio.client.Client()
        
        rule_ids = client.add_replication_rule(dids=elements,
                                               copies=1,
                                               comment=comment,
                                               rse_expression=rse)
    except rucio.common.exception.DuplicateRule:
        print("a rule for "+",".join([e["name"] for e in elements])+" already exists "+comment)
        return False
    except rucio.common.exception.CannotAuthenticate:
        raise RuntimeError("you need to call 'voms-proxy-init -voms atlas' first")
    print("adding rule for "+",".join([e["name"] for e in elements]) + " "+comment)
    return True

def get_lfn(guid):
    """obtain the LFN associated to a given GUID"""
    # based on https://twiki.cern.ch/twiki/pub/AtlasComputing/Atlantis/getRucioLFNbyGUID.py.txt
    import hashlib 
    import sys
    import rucio.client
    import rucio.common.exception
    
    try:
        client=rucio.client.Client()
        
        did = client.get_dataset_by_guid(guid)
        for d in did:
            lfn=d['name']
            scope=d['scope']
            dsname=d['name']
        
            for file in client.list_files(scope=scope, name=lfn):
                fname = file['name']
                guid2 = file['guid'].upper()
                guid2 = '{:s}-{:s}-{:s}-{:s}-{:s}'.format(guid2[0:8], guid2[8:12], guid2[12:16], guid2[16:20], guid2[20:32])
                #print guid2, fname
                if scope == "panda":
                    continue
                if guid2 == guid:
                    return {"guid":guid,"scope":scope,"dataset":dsname,"name":fname}
    except rucio.common.exception.CannotAuthenticate:
        raise RuntimeError("you need to call 'voms-proxy-init -voms atlas' first")

def main(args):
    """the main function"""
    import re
    events = []
    if args.infile:
        with open(args.infile) as infile:
            for line in infile:
                events.append(list(map(int,line.split())))
    for event in args.events:
        events.append(event)
    
    rules = 0
    files = []
    info = []
    for event in events:
        guid = get_guid(runno=event[0],evtno=event[1],regex=re.compile(args.regex),remote=args.remote_hadoop)
        lfn = get_lfn(guid)
        rtag = get_rtag(event[0],args.datatype)
        filename = lfn["name"]
        info.append([event[0],event[1],rtag,filename])
        files.append(filename)
        if add_rule([{"scope":lfn["scope"],"name":filename}],args.rse,"for event picking. reconstruct with "+rtag):
            rules = rules +1
    if rules > 1:
        print("replicating. visit 'https://rucio-ui.cern.ch/r2d2' to track progress!")
    else:
        print("no replications needed. check 'https://rucio-ui.cern.ch/r2d2' to see your rules!")
    print("once your files are replicated, run")
    print("    rucio download " + " ".join(files))

    if args.write:
        with open(args.write,"wt") as writefile:
            for line in info:
                writefile.write(" ".join(line)+"\n")
        print("wrote updated info to "+args.writefile)


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="transfer datasets containing event numbers to an RSE")
    parser.add_argument("--list",type=str,dest="infile",default=None,help="list of events. file with 'runnumber eventnumber' per line")
    parser.add_argument("--event",type=str,action="append",dest="events",nargs=2,default=[],help="a single event",metavar=("runnumber","eventnumber"))
    parser.add_argument("--regex",type=str,default=".*data.*_.*TeV\.physics_Main\..*",help="regex matching for the container name")
    parser.add_argument("--datatype",type=str,default="DAOD_HIGG3D1",help="name of the derived AOD from which to pick up the tags")
    parser.add_argument("--rse",type=str,required=True,help="RSE to replicate the files to")
    parser.add_argument("--write",type=str,help="write updated event list with reco tags to a new file")
    parser.add_argument("--remote-hadoop",action="store_true",help="use remote version of hadoop",default=False)
    args = parser.parse_args()
    if not args.infile and not args.events:
        print("you have to provide at least one of '--list' and '--event'")
    else:
        main(args)
